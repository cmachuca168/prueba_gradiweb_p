<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\OwnerController;
use App\Http\Controllers\VehicleController;
use App\Http\Controllers\BrandController;
use App\Http\Controllers\TypeVehicleController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::apiresource('/owners', OwnerController::class);
Route::apiresource('/vehicles', VehicleController::class);
Route::apiresource('/brands', BrandController::class);
Route::apiresource('/types', TypeVehicleController::class);

Route::get('/count-brands','App\Http\Controllers\BrandController@countBrands');
