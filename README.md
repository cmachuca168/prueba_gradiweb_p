Los pasos a seguir son:

1. Clonar repositorio.
2. Lanzar comando Composer install
3. Lanzar comando npm install
4. Lanzar comando npm run watch
5. Revisar el archivo .env para validar nombre de la base de datos y crearla en MySQL
6. Lanzar comando php artisan migrate:fresh --seed
7. Lanzar comando php artisan serve