<?php

namespace App\Http\Controllers;

use App\Models\Vehicle;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

 
        $filter = $request->buscador;

        // $vehicles = Vehicle::filter($filter)->get();

        // return $vehicles;

        $vehicles = DB::table('vehicles')
        ->join('owners', 'vehicles.owner_id', '=', 'owners.id')
        ->join('brands', 'vehicles.brand_id', '=', 'brands.id')
        ->join('type_vehicles', 'vehicles.type_vehicle_id', '=', 'type_vehicles.id')
        ->select('vehicles.*', 'owners.*', 'brands.*', 'type_vehicles.*', 'vehicles.id as vehicle_id')
        ->where('plate', 'LIKE',  '%'.$filter.'%')
        ->orWhere('name_brand', 'LIKE',  '%'.$filter.'%')
        ->orWhere('type_vehicle', 'LIKE',  '%'.$filter.'%')
        ->orWhere('first_name', 'LIKE',  '%'.$filter.'%')
        ->orWhere('last_name', 'LIKE',  '%'.$filter.'%')
        ->orWhere('document', 'LIKE',  '%'.$filter.'%')
        ->get();

        return ucfirst(strtolower($vehicles));
        // return Vehicle::get();
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->plate === null || $request->brand === null || $request->type === null || $request->owner === null) {
            return response()->json(['error' => 'Revise que no halla ningun campo vacio'], 500);
        }

        $vehicle = Vehicle::create([
            'plate' => $request->plate,
            'brand_id' => $request->brand['id'],
            'type_vehicle_id' => $request->type['id'],
            'owner_id' => $request->owner['id'],
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vehicle $vehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicle $vehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicle  $vehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicle $vehicle)
    {
        $vehicle->delete();
    }
}
