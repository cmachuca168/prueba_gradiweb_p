<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vehicle extends Model
{
    
    protected $fillable = [
        'plate',
        'brand_id',
        'type_vehicle_id',
        'owner_id',
    ];


    use HasFactory;
}
