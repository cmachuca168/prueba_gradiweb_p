<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeVehicleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('type_vehicles')->insert([
            'type_vehicle' => 'Autobus'
        ]);

        DB::table('type_vehicles')->insert([
            'type_vehicle' => 'Motocicleta'
        ]);

        DB::table('type_vehicles')->insert([
            'type_vehicle' => 'Automóvil'
        ]);

        DB::table('type_vehicles')->insert([
            'type_vehicle' => 'Camioneta'
        ]);

        DB::table('type_vehicles')->insert([
            'type_vehicle' => 'Camión'
        ]);
    }
}
