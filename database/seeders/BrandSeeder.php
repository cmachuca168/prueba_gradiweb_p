<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('brands')->insert([
            'name_brand' => 'Renault'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'Chevrolet'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'Suzuki'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'Toyota'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'Volkswagen'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'AKT'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'BAJAJ'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'DUCATI'
        ]);

        DB::table('brands')->insert([
            'name_brand' => 'HONDA'
        ]);
    }
}
